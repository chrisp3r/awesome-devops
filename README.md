## Awesome DevOps

This is an all-you-can-eat buffet of information for working in DevOps for the web. Start with something you don't know, but this list is ordered from a logical beginning to end. You don't have to learn every detail about every technology here, just be exposed to the general concepts and know how to dig deeper when you need more information.

### Ubuntu and Linux:
Pluralsight:  
https://app.pluralsight.com/library/courses/ubuntu-getting-started/table-of-contents

### Linux objectives from CompTIA:  
http://cis.ncu.edu.jm/comptia/CompTIA%20Linux%20Powered%20by%20LPI_LX0-103.pdf  
http://cis.ncu.edu.jm/comptia/CompTIA%20Linux%20Powered%20by%20LPI_LX0-104.pdf  

### Bash basics, the swiss army knife:  
https://www.codecademy.com/catalog/language/bash

### Web App Basics  
What is DNS?  
https://computer.howstuffworks.com/dns.htm  
the guts of HTTP:  
https://www.w3schools.com/tags/ref_httpmethods.asp  
HTTP Headers:  
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers  
What is a RESTful API?  
https://searchmicroservices.techtarget.com/definition/RESTful-API  
RESTful API authentication:  
https://blog.restcase.com/restful-api-authentication-basics/  
RESTful API tool:  
https://www.getpostman.com/  
Base64 encoder/decoder:  
https://www.base64encode.org/  
What is JSON?  
https://developers.squarespace.com/what-is-json  
JSON tool:  
http://jsoneditoronline.org/  

### Docker training  
Pluralsight:  
https://app.pluralsight.com/library/courses/docker-web-development/table-of-contents  
Your best friend, documentation:  
https://docs.docker.com/  

### Kubernetes training
Pluralsight:  
https://app.pluralsight.com/library/courses/getting-started-kubernetes/table-of-contents  

### Rancher training  
YouTube:  
https://www.youtube.com/watch?v=rbIplqthwbQ  
Checkout Rancher YouTube channel:  
https://www.youtube.com/channel/UCh5Xtp82q8wjijP8npkVTBA  
and maybe Rancher Slack channel:  
https://slack.rancher.io/  
Rancher 2.x docs:  
https://rancher.com/docs/rancher/v2.x/en/  

### Ruby basics:  
https://www.codecademy.com/learn/learn-ruby  

### Advanced Ruby:  
make your life easier with the Ruby Version Manager:  
https://rvm.io/  
This lets install any number of Ruby versions and use them when needed:  
`rvm install 2.3.1`  
`rvm use 2.3.1`  

### Ruby Gems:  
What are they?  
https://stackoverflow.com/questions/5233924/what-is-a-ruby-gem  
https://rubygems.org/  
Ruby dependency management:  
https://bundler.io/  

### Git basics:
Security tip: never store passwords in Git  
https://www.codecademy.com/learn/learn-git  
Pro Tip: Use git branches and merge or pull requests when making changes  

### more Git:
https://www.udacity.com/course/how-to-use-git-and-github—ud775  

### CI/CD  
What is it?   https://www.infoworld.com/article/3271126/ci-cd/what-is-cicd-continuous-integration-and-continuous-delivery-explained.html  

### PCF basics:  
https://docs.cloudfoundry.org/cf-cli/index.html  

### TeamCity training  
Pluralsight:  
https://app.pluralsight.com/library/courses/teamcity-getting-started/table-of-contents  

### Configuration Management  
What is it?  
https://www.netapp.com/us/info/what-is-configuration-management.aspx  
*TL;DR:* Store your infrastructure / deployments as code in Git. All changes made to code or deployments are now tracked and initiated via Git.

### Configuration Management - Chef training:  
Pluralsight:  
https://app.pluralsight.com/library/courses/chef-cookbooks-getting-productive/table-of-content  

I have not reviewed this:
https://app.pluralsight.com/library/courses/chef-planning-installing/table-of-contents  

## Extra credit:*

### What is a proxy server?  
https://www.webopedia.com/TERM/P/proxy_server.html  

### NGINX:  
https://www.nginx.com/resources/glossary/nginx/  

### HAProxy:  
https://www.digitalocean.com/community/tutorials/an-introduction-to-haproxy-and-load-balancing-concepts  

### MongoDB:  
Pluralsight:  
https://app.pluralsight.com/library/courses/mongodb-introduction  
Mongo tool:  
https://robomongo.org/  

### Python basics:  
https://www.codecademy.com/catalog/language/python  

### JavaScript basics / Mongo:  
https://www.codecademy.com/catalog/language/javascript  

### Golang basics:  
https://tour.golang.org/welcome/1  


### Security

http vs https:  
https://www.instantssl.com/ssl-certificate-products/https.html  
TLS:  
https://en.wikipedia.org/wiki/Transport_Layer_Security  
Kubernetes secrets:  
https://cloud.google.com/kubernetes-engine/docs/concepts/secret  
HashiCorp Vault:  
https://www.vaultproject.io/docs/  
